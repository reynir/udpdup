open Lwt.Infix

let port = 2233

let rec loop fd dests =
  let recv_buf = Bytes.make 2048 '\000' in
  Lwt_unix.recv fd recv_buf 0 (Bytes.length recv_buf) [] >>= fun read_len ->
  Lwt_list.iter_p
    (fun fd ->
       Lwt_unix.send fd recv_buf 0 read_len [] >>= fun _send_len ->
       Lwt.return_unit)
    dests >>= fun () ->
  loop fd dests

let main () =
  let addr = "0.0.0.0" in
  let dests =
    Array.to_list Sys.argv
    |> List.tl
    |> List.map (fun dest ->
        match String.split_on_char ':' dest with
        | [host; port] ->
          host, int_of_string port
        | _ ->
          Printf.eprintf "bad argument: %s" dest;
          exit 1)
  in
  let fd = Lwt_unix.socket Lwt_unix.PF_INET Lwt_unix.SOCK_DGRAM 0 in
  Lwt_unix.bind fd Unix.(ADDR_INET (inet_addr_of_string addr, port)) >>= fun () ->
  Lwt_list.map_p (fun (addr, port) ->
      let socket = Lwt_unix.socket Unix.PF_INET Unix.SOCK_DGRAM 0 in
      Lwt_unix.connect socket Unix.(ADDR_INET (inet_addr_of_string addr, port)) >>= fun () ->
      Lwt.return socket)
    dests >>= fun dests ->
  loop fd dests

let () = Lwt_main.run (main ())
